({
	doInit : function(c, e, h) {
		h.doInit_helper(c, e, h);
	},
    handleClick : function(c, e, h) {
		h.handleClick_helper(c, e, h);
	},
    cloneQuote : function(c, e, h) {
		h.cloneQuote_helper(c, e, h);
	},
	closeToast : function(c, e, h) {
        h.closeToast_helper(c, e, h);
    },
})