({
	doInit_helper : function(c, e, h) {
        var recId = c.get('v.recordId');
        console.log('Recc'+recId);
	},
    handleClick_helper : function(c, e, h) {
        /*var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();*/
        var recId = c.get('v.recordId');
        var url = 'https://'+location.hostname + '/' +recId;
        window.open(url, '_self');
	},
    cloneQuote_helper : function(c, e, h) {
        var descriptionName = c.get('v.descriptionFields');
        console.log(descriptionName);
        var recordId = c.get('v.recordId');
        h.getCloneRecords_helper(c, e, h, recordId, descriptionName);
	},
    getCloneRecords_helper : function(c, e, h, recordId, descriptionName) {
        try {
            c.set('v.setSpinnerTrueFalse', true);
            var action = c.get("c.CloneWithChildrenCtrlComponent_Apex");
            action.setParams({
                recordId : recordId,
                descriptionName : descriptionName
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state === 'SUCCESS'){
                    var resp = response.getReturnValue();
                    console.log('resp->>>'+JSON.stringify(resp));
                    if(!$A.util.isEmpty(resp) && resp != undefined) {
                        var cloneRecId = resp[0].Id;
                        c.set('v.isMessage', true);
                        c.set('v.successMessage', 'Your data has been Cloned Successfully');
                        c.set('v.descriptionFields', '');
                        var url = 'https://'+location.hostname + '/' +cloneRecId;
                        window.open(url);
                        window.setTimeout(function () {
                            h.closeToast_helper(c, e, h);
                        },5000);
                    } else {
                        console.log('Not Cloned Something Happen Verity. Return Value Null');
                    }
                    c.set('v.setSpinnerTrueFalse', false);
                }else if (state === 'ERROR'){
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                            c.set('v.isMessage', true);
                            c.set('v.errorMessage', errors[0].message);
                            window.setTimeout(function () {
                                h.closeToast_helper(c, e, h);
                            },5000);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                    c.set('v.setSpinnerTrueFalse', false);
                }else{
                    console.log('Something went wrong, Please check with your admin');
                }
            });
            $A.enqueueAction(action);
        } catch(ex){
            console.log(ex);
        }
    },
    showToast_helper : function(c, e, h, title, message, msgType) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: message,
            messageTemplate: 'Record {0} created! See it {1}!',
            duration:' 5000',
            key: 'info_alt',
            type: msgType,
            mode: 'pester'
        });
        toastEvent.fire();
    },
    closeToast_helper : function(c, e, h) {
        c.set('v.isMessage', false);
        c.set('v.successMessage', '');
        c.set('v.errorMessage', '');
    },
})